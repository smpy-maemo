from distutils.core import setup

setup(name='smpy',
      version='2008.3.1',
      scripts=['smpy'],
      packages=['mechanize'],
      modules=['ClientForm', 'robotparser'],
      py_modules=['ClientForm', 'robotparser'],
      data_files=[
          ('share/applications/hildon', ['smpy.desktop']),
          ('share/icons/hicolor/26x26/hildon', ['icons/26x26/smpy.png']),
          ('share/icons/hicolor/40x40/hildon', ['icons/40x40/smpy.png']),
          ('share/icons/hicolor/scalable/hildon', ['icons/scalable/smpy.png']),
      ])


